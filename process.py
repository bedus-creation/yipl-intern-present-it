import pandas as pd

survey_data = pd.read_csv("survey.csv")
df=pd.DataFrame(survey_data,columns=['Gender','District'])
is_male=survey_data[survey_data["Gender"]=="male"]
is_female=survey_data[survey_data["Gender"]=="female"]
df1=is_male.groupby(["District"]).size().rename_axis("Gender").reset_index(name="Male")
df2=is_female.groupby(["District"]).size().rename_axis("Gender").reset_index(name="female")
df4=df.groupby(["District"]).size().rename_axis("Gender").reset_index(name="Total")
df3=pd.merge(df2,df1)
df5=pd.merge(df3,df4)
print(df5)
df7=pd.DataFrame(df5,columns=['Male','female','Total'])
print(df7.sum())

